# Publicaciones Hacker News
Este sitio corresponde a un Test de Backend para Java Develover Full Stack. Se descarga cada hora (de forma automática) el listado de publicaciones disponibles del sitio Hacker News.

## Lenguajes/Herramientas usadas
- Java 13
- Spring Boot 2.2.4
- Maven

## Instalación y ejecución
1. Clonar el repositorio.
git clone https://cesar3r2@bitbucket.org/cesar3r2/publicaciones-back.git

2. Crear la base de datos y migrar la base de datos (en la sección siguiente se encuentran los datos).
- Opción 1. En la carpeta /src/bd se encuentra un respaldo (extensión .sql) con la estructura con algunos datos de prueba, se puede importar directamente en el manejador de bd.
- Opción 2: Solo crear la bd en el manejador y al ejecutarse por primera vez, la migración de la estructura se realizará de manera automática por estar mapeado con JPA.

3. Desde el IDE de tu preferencia ejecutar Run para levantar el servicio, por defecto se asigna el puerto 8080. El backend de la aplicación estará disponible en http://localhost:8080.

## Configuración de la base de datos
Majenador usado: MYSQL
Host: localhost
Puerto: 3306
Nombre bbdd: ig_publicaciones
Codificación: UTF-8 general
Username: root
Password: 12345678

## Rutas creadas para la API Hacker News

# Obtener las publicaciones desde la API Hacker News (se tomó como campo clave el atributo objectID)
- GET | http://localhost:8080/api/v1/publicaciones/getfromapi

# Obtener las publicaciones desde la BD local (el atributo objectID de la API se guarda en el campo story_id)
- GET | http://localhost:8080/api/v1/publicaciones/all

# Eliminar una publicación de la BD local
- DELETE | http://localhost:8080/api/v1/publicaciones/delete/{story_id}
