/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : ig_publicaciones

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 06/02/2020 12:13:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(20) NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES (16);

-- ----------------------------
-- Table structure for publicacion
-- ----------------------------
DROP TABLE IF EXISTS `publicacion`;
CREATE TABLE `publicacion`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `story_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `titulo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `autor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fecha` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of publicacion
-- ----------------------------
INSERT INTO `publicacion` VALUES (2, '22252880', 'Scaling to 100k Users', 'holoduke', '2020-02-06 02:11:33', '2020-02-06 10:22:06');
INSERT INTO `publicacion` VALUES (3, '22252286', 'Scaling to 100k Users', 'tqkxzugoaupvwqr', '2020-02-06 00:19:22', '2020-02-06 10:22:06');
INSERT INTO `publicacion` VALUES (4, '22252105', 'YaCy: Decentralized Web Search', 'bawolff', '2020-02-05 23:50:14', '2020-02-06 10:22:06');
INSERT INTO `publicacion` VALUES (5, '22251804', 'Bocker – Docker implemented in around 100 lines of Bash (2015)', 'kazinator', '2020-02-05 22:59:51', '2020-02-06 10:22:07');
INSERT INTO `publicacion` VALUES (7, '22251270', 'Bocker – Docker implemented in around 100 lines of Bash (2015)', 'jrockway', '2020-02-05 21:42:18', '2020-02-06 10:22:07');
INSERT INTO `publicacion` VALUES (8, '22251165', 'End-of-Life Announcement for CoreOS Container Linux', 'hosh', '2020-02-05 21:28:35', '2020-02-06 10:22:07');
INSERT INTO `publicacion` VALUES (14, '22255956', 'Babashka – A Clojure babushka for the grey areas of Bash', 'arnautdaniel', '2020-02-06 14:01:10', '2020-02-06 11:33:36');

-- ----------------------------
-- Table structure for publicacion_delete
-- ----------------------------
DROP TABLE IF EXISTS `publicacion_delete`;
CREATE TABLE `publicacion_delete`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `story_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `titulo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `autor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fecha` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of publicacion_delete
-- ----------------------------
INSERT INTO `publicacion_delete` VALUES (10, '22253335', 'null', 'feross', '2020-02-06 04:03:38', '2020-02-06 10:44:39');
INSERT INTO `publicacion_delete` VALUES (11, '22250587', 'null', 'bryanrasmussen', '2020-02-05 20:18:04', '2020-02-06 10:44:44');
INSERT INTO `publicacion_delete` VALUES (12, '22251354', 'null', 'likhuta', '2020-02-05 21:53:19', '2020-02-06 10:44:48');
INSERT INTO `publicacion_delete` VALUES (15, '22256088', 'null', 'lirantal', '2020-02-06 14:19:24', '2020-02-06 11:34:33');

SET FOREIGN_KEY_CHECKS = 1;
