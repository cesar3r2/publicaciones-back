package com.ingerencia.publicaciones;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.time.LocalDateTime;

@Entity // This tells Hibernate to make a table out of this class
public class Publicacion {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;
    private String story_id;
    private String titulo;
	private String autor;
	private LocalDateTime fecha;
    private LocalDateTime created_at;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
    }
    
	public String getStoryId() {
		return story_id;
	}
	public void setStoryId(String story_id) {
		this.story_id = story_id;
    }
    
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
    }
    
    public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
    }

    public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
    }

    public LocalDateTime getCreatedAt() {
		return created_at;
	}
	public void setCreatedAt(LocalDateTime created_at) {
		this.created_at = created_at;
	}
	
	@Override
    public String toString() {
        return "Publicacion{" +
                "id=" + id +
                ", story_id='" + story_id + '\'' +
                ", titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
				", fecha=" + fecha +
				", created_at=" + created_at +
                '}';
    }
}
