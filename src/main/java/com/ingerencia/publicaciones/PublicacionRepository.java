package com.ingerencia.publicaciones;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ingerencia.publicaciones.Publicacion;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PublicacionRepository extends CrudRepository<Publicacion, Integer> {
    @Query(value = "SELECT * FROM publicacion WHERE story_id=:story_id LIMIT 1", nativeQuery = true)
    Publicacion getPubByStoryId(@Param("story_id") String story_id);
}
