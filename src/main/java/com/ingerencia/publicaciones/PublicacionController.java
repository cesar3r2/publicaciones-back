package com.ingerencia.publicaciones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.scheduling.annotation.Scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(path = "/api/v1/publicaciones")
public class PublicacionController {
    private static final Logger logger = LoggerFactory.getLogger(PublicacionController.class);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Autowired
    public PublicacionRepository publicacionRepository;
    @Autowired
    public PublicacionDeleteRepository publicacionDeleteRepository;

    // Método temporal para insertar una publicación
    @PostMapping(path = "/add")
    public @ResponseBody String addNewPublicacion(@RequestParam String story_id, @RequestParam String titulo,
            @RequestParam String autor, @RequestParam LocalDateTime fecha) {
        Publicacion n = new Publicacion();
        n.setStoryId(story_id);
        n.setTitulo(titulo);
        n.setAutor(autor);
        n.setFecha(fecha);
        
        publicacionRepository.save(n);
        return "registro guardado";
    }

    // Método para listar las publicaciones de Hacker News
    @GetMapping(path = "/all", produces = "application/json")
    public @ResponseBody Iterable<Publicacion> getAllPublicaciones() {
        return publicacionRepository.findAll();
    }

    // Método para descargar desde la api la bd local las publicaciones de Hacker News
    @Scheduled(cron = "0 0 * * * ?") // Se ejecuta el método cada hora
    @GetMapping(path = "/getfromapi")
    public ResponseEntity<String> getApiPublicaciones() {
        String url = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(url, String.class);
        
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode node = mapper.readTree(response);
            for (int i=0; i<node.size(); i++) {
                // Valida que las publicaciones a registrar en la bd no estén duplicadacas ni eliminadas por el usuario anteriormente
                if (publicacionRepository.getPubByStoryId(node.path("hits").get(i).path("objectID").asText()) == null && publicacionDeleteRepository.getPubByStoryId(node.path("hits").get(i).path("objectID").asText()) == null) {
                    Publicacion pub = new Publicacion();
                    pub.setStoryId(node.path("hits").get(i).path("objectID").asText());
                    pub.setTitulo(node.path("hits").get(i).path("story_title").asText());
                    pub.setAutor(node.path("hits").get(i).path("author").asText());
                    String fecha = node.path("hits").get(i).path("created_at").asText();
                    DateTimeFormatter isoFormatter = DateTimeFormatter.ISO_INSTANT;
                    Instant dateInstant = Instant.from(isoFormatter.parse(fecha));
                    LocalDateTime date = LocalDateTime.ofInstant(dateInstant, ZoneId.of(ZoneOffset.UTC.getId()));
                    pub.setFecha(date);
                    pub.setCreatedAt(LocalDateTime.now());
                    // Registra cada publicación
                    publicacionRepository.save(pub);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        logger.info("Actualización de Publicaciones: - {}", formatter.format(LocalDateTime.now()));
        return new ResponseEntity<>("Las publicaciones se han actualizado correctamente", HttpStatus.OK);
    }
    
    // Método para eliminar publicaciones de Hacker News en la bd local
    @DeleteMapping(path = "/delete/{id}")
    public @ResponseBody String deletePublicacion(@PathVariable("id") int id) {
        Optional<Publicacion> p = publicacionRepository.findById(id);
        
        PublicacionDelete pubDel = new PublicacionDelete();
        pubDel.setStoryId(p.get().getStoryId());
        pubDel.setTitulo(p.get().getTitulo());
        pubDel.setAutor(p.get().getAutor());
        pubDel.setFecha(p.get().getFecha());
        pubDel.setCreatedAt(LocalDateTime.now());
        // Inserta en la tabla publicacion_delete el registro antes de eliminarlo
        publicacionDeleteRepository.save(pubDel);
        // Elimina el registro
        publicacionRepository.deleteById(id);

        return "Publicación eliminada con éxito";
    }
}
