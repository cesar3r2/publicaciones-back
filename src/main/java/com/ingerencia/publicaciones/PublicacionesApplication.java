package com.ingerencia.publicaciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PublicacionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PublicacionesApplication.class, args);
	}

	// Permite ejecución CORS desde el front
	@Bean
	public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
					registry.addMapping("/api/v1/publicaciones/**")
							.allowedOrigins("http://localhost:4200")
							.allowedMethods("GET", "POST", "PUT", "DELETE")
							.maxAge(3600);
			}
		};
	}
}
