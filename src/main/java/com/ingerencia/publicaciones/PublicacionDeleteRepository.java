package com.ingerencia.publicaciones;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ingerencia.publicaciones.PublicacionDelete;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PublicacionDeleteRepository extends CrudRepository<PublicacionDelete, Integer> {
    @Query(value = "SELECT * FROM publicacion_delete WHERE story_id=:story_id LIMIT 1", nativeQuery = true)
    PublicacionDelete getPubByStoryId(@Param("story_id") String story_id);
}
